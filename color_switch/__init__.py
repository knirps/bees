from st3m.application import Application, ApplicationContext
from st3m.input import InputController
import st3m.run
import leds

colors = [
        (255, 0, 0),
        (255, 255, 0),
        (0, 255, 0),
        (0, 255, 255),
        (0, 0, 255),
        (255, 0, 255),
]

class ColorSwitch(Application):
    def __init__(self, ctx: ApplicationContext) -> None:
        super().__init__(ctx)
        self.input = InputController()
        self.color = 0

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.move_to(0,0)
        (r,g,b) = colors[self.color]
        ctx.rgb(r,g,b)
        ctx.font = "Camp Font 1"
        ctx.text(f"{self.color}")

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if self.input.buttons.app.left.pressed:
            self.color = (self.color + 1) % len(colors)
        elif self.input.buttons.app.right.pressed:
            self.color = (self.color - 1) % len(colors)

        (r, g, b) = colors[self.color]
        leds.set_all_rgb(r, g, b)
        leds.update()

if __name__ == "__main__":
    st3m.run.run_view(ColorSwitch(ApplicationContext()))
